package ictgradschool.industry.lab17.ex01;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RobotTest {

    private Robot myRobot;

    @Before
    public void setUp() {
        myRobot = new Robot();
    }

    @Test
    public void testRobotConstruction() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void testIllegalMoveNorth() {
        boolean atTop = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();
            // Check that robot has reached the top.
            atTop = myRobot.currentState().row == 1;
            assertTrue(atTop);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().row);
        }
    }

    @Test
    public void testIllegalMoveSouth() {
        boolean atBottom = false;

        while (myRobot.getDirection() != Robot.Direction.South) {
            myRobot.turn();
        }

        try {
            // Now try to continue to move South.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().row);
        }
    }

    @Test
    public void testIllegalMoveEast() {
        boolean atEast = false;

        while (myRobot.getDirection() != Robot.Direction.East) {
            myRobot.turn();
        }

        try {
            // Now try to continue to move South.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();
            // Check that robot has reached the top.
            atEast = myRobot.currentState().column == 10;
            assertTrue(atEast);
        } catch (IllegalMoveException e) {
            fail();
        }
        try {
            // Now try to continue to move South.
            myRobot.move();
            fail();

        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(10, myRobot.currentState().column);
        }
    }

    @Test
    public void testIllegalMoveWest() {
        boolean atWest = false;

        while (myRobot.getDirection() != Robot.Direction.West) {
            myRobot.turn();
        }

        try {
            // Now try to continue to move South.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().column);
        }
    }

    @Test
    public void testValidMoveNorth() {
        try {
            // Now try to continue to move North.
            myRobot.move();
            assertEquals(9, myRobot.currentState().row);
        } catch (IllegalMoveException e) {
            fail();
            // Ensure the move didn’t change the robot state
        }
    }

    @Test
    public void testValidMoveSouth() {
        try {
            // Now try to continue to move North.
            myRobot.move();

            while (myRobot.getDirection() != Robot.Direction.South) {
                myRobot.turn();
            }

            myRobot.move();
            assertEquals(10, myRobot.currentState().row);
        } catch (IllegalMoveException e) {
            fail();
            // Ensure the move didn’t change the robot state
        }
    }

    @Test
    public void testValidMoveEast() {
        try {
            // Now try to continue to move North.
            while (myRobot.getDirection() != Robot.Direction.East) {
                myRobot.turn();
            }

            myRobot.move();
            assertEquals(2, myRobot.currentState().column);
        } catch (IllegalMoveException e) {
            fail();
            // Ensure the move didn’t change the robot state
        }
    }

    @Test
    public void testValidMoveWest() {
        try {
            while (myRobot.getDirection() != Robot.Direction.East) {
                myRobot.turn();
            }
            // Now try to continue to move North.
            myRobot.move();

            while (myRobot.getDirection() != Robot.Direction.West) {
                myRobot.turn();
            }

            myRobot.move();
            assertEquals(1, myRobot.currentState().column);
        } catch (IllegalMoveException e) {
            fail();
            // Ensure the move didn’t change the robot state
        }
    }

    @Test
    public void testTurnNorth() {
        while (myRobot.getDirection() != Robot.Direction.West) {
            myRobot.turn();
        }
        myRobot.turn();
        assertEquals(Robot.Direction.North, myRobot.getDirection());

    }

    @Test
    public void testTurnSouth() {
        while (myRobot.getDirection() != Robot.Direction.East) {
            myRobot.turn();
        }

        myRobot.turn();
        assertEquals(Robot.Direction.South, myRobot.getDirection());
    }

    @Test
    public void testTurnEast() {
        while (myRobot.getDirection() != Robot.Direction.North) {
            myRobot.turn();
        }

        myRobot.turn();

        assertEquals(Robot.Direction.East, myRobot.getDirection());
    }

    @Test
    public void testTurnWest() {
        while (myRobot.getDirection() != Robot.Direction.South) {
            myRobot.turn();
        }
        myRobot.turn();
        assertEquals(Robot.Direction.West, myRobot.getDirection());
    }

    @Test
    public void testBackTrack() {
        Robot.RobotState state = myRobot.currentState();
        try {
            myRobot.move();
            myRobot.backTrack();
            assertEquals(state, myRobot.states.get(myRobot.states.size() - 1));
        } catch (IllegalMoveException e) {
            fail();
        }
    }

}

