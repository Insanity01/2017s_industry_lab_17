package ictgradschool.industry.lab17.ex02;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by vwen239 on 15/01/2018.
 */
public class DictionaryTest {
    private Dictionary dictionary;

    @Before
    public void setUp(){
        dictionary = new Dictionary();
    }

    @Test
    public void testIsSpellingWrong() {
        assertFalse(dictionary.isSpellingCorrect("gdsfyj"));
    }
    @Test
    public void testIsSpellingCorrect() {
        assertTrue(dictionary.isSpellingCorrect("the"));
    }

}