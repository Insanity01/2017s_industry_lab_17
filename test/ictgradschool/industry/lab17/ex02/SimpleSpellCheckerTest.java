package ictgradschool.industry.lab17.ex02;

import org.junit.*;

import java.util.Arrays;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by vwen239 on 15/01/2018.
 */
public class SimpleSpellCheckerTest {
    private Dictionary dictionary;
    private SimpleSpellChecker simpleChecker;


    @Before
    public void setUp(){
        dictionary = new Dictionary();
        try {
            simpleChecker = new SimpleSpellChecker(dictionary, "thtwhw hello hello hi");
        } catch (InvalidDataFormatException e) {

        }
    }
    @Test
    public void testNotNull(){
        try {
            SimpleSpellChecker testChecker = new SimpleSpellChecker(dictionary, null);
            fail();
        } catch (InvalidDataFormatException e) {
            assertTrue(true);
        }
    }

    @Test
    public void testMisspelledWords(){
        assertEquals("[thtwhw]", Arrays.toString(simpleChecker.getMisspelledWords().toArray()));
    }

    @Test
    public void testUniqueWords() {
        assertEquals("[hi, thtwhw, hello]", Arrays.toString(simpleChecker.getUniqueWords().toArray()));
    }

    @Test
    public void testFrequencyOfWord() {
        try {
            assertEquals(2, simpleChecker.getFrequencyOfWord("hello"));
        } catch (InvalidDataFormatException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void testInvalidFrequencyOfWord() {
        try {
            assertEquals(0, simpleChecker.getFrequencyOfWord("happy"));
        } catch (InvalidDataFormatException e) {
            e.printStackTrace();
        }
    }


}